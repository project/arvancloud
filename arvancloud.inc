<?php

/**
 * @file
 * Contains the main purging functionality.
 */

/**
 * Purge all cache from ArvanCloud.
 */
function arvancloud_purge_all() {
  $is_set = arvancloud_check_variables_set();
  if (!$is_set) {
    return;
  }

  $url = format_string('@endpoint/@domain_key/purge-cache', array(
    '@endpoint' => variable_get('arvancloud_endpoint', 'https://www.arvancloud.com/api/v1/domains'),
    '@domain_key' => variable_get('arvancloud_domain_key', ''),
  ));

  $options = array(
    'method' => 'DELETE',
    'headers' => array(
      'user' => variable_get('arvancloud_username', ''),
      'key' => variable_get('arvancloud_api_key', ''),
      'cmd' => 'purge-all',
    ),
  );

  $result = drupal_http_request($url, $options);
  arvancloud_log($result);
}

/**
 * Purge one file (url or file) from ArvanCloud.
 */
function arvancloud_purge_files($file) {
  $is_set = arvancloud_check_variables_set();
  if (!$is_set) {
    return;
  }

  $url = format_string('@endpoint/@domain_key/purge-cache', array(
    '@endpoint' => variable_get('arvancloud_endpoint', 'https://www.arvancloud.com/api/v1/domains'),
    '@domain_key' => variable_get('arvancloud_domain_key', ''),
  ));

  $options = array(
    'method' => 'DELETE',
    'headers' => array(
      'user' => variable_get('arvancloud_username', ''),
      'key' => variable_get('arvancloud_api_key', ''),
      'cmd' => 'purge-files',
      'Content-Type' => 'application/x-www-form-urlencoded',
    ),
    'data' => array(
      'files' => 'files=' . $file,
    ),
  );

  $result = drupal_http_request($url, $options);
  arvancloud_log($result);
}

/**
 * If all ArvanCloud variables was set, return true.
 *
 * @return bool
 *   If all variables was set, return TRUE, otherwise return FALSE.
 */
function arvancloud_check_variables_set() {
  $api_key = variable_get('arvancloud_api_key', '');
  $domain_key = variable_get('arvancloud_domain_key', '');
  $username = variable_get('arvancloud_username', '');
  $endpoint = variable_get('arvancloud_endpoint', '');

  if (!(empty($api_key) ||
      empty($domain_key) ||
      empty($username) ||
      empty($endpoint))) {
    return TRUE;
  }

  return FALSE;
}

/**
 * After Purging, log results to database.
 */
function arvancloud_log($result) {
  $result = drupal_json_decode($result->data);

  watchdog('arvancloud', "ArvanCloud executed command <br/><pre>@result</pre>", array(
    '@result' => print_r($result, TRUE),
  ));
}
