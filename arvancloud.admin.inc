<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Menu callback for ArvanCloud admin settings.
 */
function arvancloud_admin_settings_form($form, &$form_state) {

  $form['arvancloud_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('ArvanCloud API Key'),
    '#default_value' => variable_get('arvancloud_api_key', ''),
    '#description' => t("Enter your api-key for ArvanCloud's API service."),
    '#required' => TRUE,
  );

  $form['arvancloud_domain_key'] = array(
    '#type' => 'textfield',
    '#title' => t('ArvanCloud Domain Key'),
    '#default_value' => variable_get('arvancloud_domain_key', ''),
    '#description' => t("Enter your domain-key for ArvanCloud's API service."),
    '#required' => TRUE,
  );

  $form['arvancloud_username'] = array(
    '#type' => 'textfield',
    '#title' => t('ArvanCloud Username'),
    '#default_value' => variable_get('arvancloud_username', ''),
    '#description' => t("Enter your username for ArvanCloud."),
    '#required' => TRUE,
  );

  $form['arvancloud_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('ArvanCloud Endpoint'),
    '#default_value' => variable_get('arvancloud_endpoint', 'https://www.arvancloud.com/api/v1/domains'),
    '#description' => t("Enter the endpoint for ArvanCloud's API."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
