[ArvanCloud](https://www.arvancloud.com/), the first public CDN, Web
accelerator and cloud security system in Iran.

## Current Features:
- Purge all cache after add new node
- Purge all cache after update node
- Purge all cache after delete node
- Purge all cache after run cron
- Purge all cache after clear all cache

## After Installation:
- Visit https://www.arvancloud.com/ and sign up to this site.
- Go to the API settings of the dashboard site.
(https://www.arvancloud.com/account/[domain]/api)
- Configure the ArvanCloud module. (/admin/config/development/arvancloud)

## Todo List:
- Integration with [Expire](https://www.drupal.org/project/expire/) module for
better performance.
